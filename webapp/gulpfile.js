var gulp = require('gulp'),
    gutil = require('gulp-util'),
    scss = require('gulp-scss'),
    header = require('gulp-header'),
    cleanCSS = require('gulp-clean-css'),
    rename = require("gulp-rename"),
    uglify = require('gulp-uglify'),
    pkg = require('./package.json'),
    useref = require('useref'),
    gulpIf = require('gulp-if');

// Compile scsc files from /scss into /css
gulp.task('scss', function () {
    return gulp
        .src('src/scss/style.scss')
        .pipe(scss())
        .pipe(gulp.dest('dist/css'))
});

// Compile scsc files from /scss into /css
gulp.task('scss:watch', function () {
    return gulp
        .watch('src/scss/style.scss')
        .pipe(scss())
        .pipe(gulp.dest('dist/css'))
});

// Minify compiled CSS
gulp.task('minify-css', ['scss'], function () {
    return gulp
        .src('src/css/style.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist/css'))
});