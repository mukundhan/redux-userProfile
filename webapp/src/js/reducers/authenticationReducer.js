import actionTypes from '../constants/actionTypes'
export default function reducer(state = {
    user: {},
    fetching: false,
    fetched: false,
    error: null,
    authenticated: false,
    role: null
}, action) {
    switch (action.type) {
        case "LOGIN":
            {
                return {
                    ...state,
                    fetching: true
                }
                break;
            }
        case "LOGIN_REJECTED":
            {
                return {
                    ...state,
                    fetching: false,
                    error: action.payload
                }
                break;
            }
        case "LOGIN_FULFILLED":
            {
                return {
                    ...state,
                    fetching: false,
                    fetched: true,
                    authenticated: true,
                    role: action.payload.userRole,
                    user: action.payload.user
                }
                break;
            }
        case "LOGOUT":
            {
                return {
                    ...state,
                    fetching: true
                }
                break;
            }
        case "LOGOUT_REJECTED":
            {
                return {
                    ...state,
                    fetching: false,
                    error: action.payload,
                    authenticated: false,
                    role: null
                }
                break;
            }
        case "LOGOUT_FULFILLED":
            {
                return {
                    ...state,
                    fetching: false,
                    fetched: true,
                    authenticated: false,
                    role: null
                }
                break;
            }
    }
    return state
}
