import {combineReducers} from 'redux'
import authenticate from './authenticationReducer'
import userProfile from './userProfileReducer'
export default combineReducers({
    authenticate,
    userProfile
},)