import Cookie from 'universal-cookie';
import constants from '../constants/constants';
import * as appUtil from '../utils/appUtil'
export function checkAuthorised() {
    const tokenId = appUtil.getCookie("tokenId");
    if (!tokenId) {
        appUtil.navigateTo(constants.LOGIN_PAGE);
        console.log("tokenId", tokenId);
    }
};
export function checkLogin() {
    const tokenId = appUtil.getCookie("tokenId");
    if (tokenId) {
        return 1;
    } else {
        return 0;
    }
};