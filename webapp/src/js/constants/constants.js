export default Object.freeze({
    ADD_USER: "ADD_USER",
    FETCH_USERS: "FETCH_USERS",
    SET_USERS: "SET_USERS",
    SET_USERS: "SET_USERS",
    DELETE_USER: "DELETE_USER",
    UPDATE_USER: "UPDATE_USER",
    HOME_PAGE: "/userProfiles",
    LOGIN_PAGE: '/login'
})