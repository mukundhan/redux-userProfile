import React from "react";
import DatePicker from 'react-bootstrap-date-picker';
import {TableSimple} from 'react-pagination-table';
import Pagination from 'react-pagination-status';
export default class Users extends React.Component {
    getFormattedDate = (date) => {
        const monthNames = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];
        const days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ];
        const formattedDate = new Date(date);
        return monthNames[formattedDate.getMonth()] + '-' + formattedDate.getDate();
    }

    render() {
        let UserNode;
        const {users} = this.props;
        const headers = ["Id", "Name", "Dob", "Age"];
        users.map(user => user.dob = this.getFormattedDate(user.dob));
        if (users) {
            UserNode = users.map((user) => {
                return (
                    <tr key={user.id}>
                        <td>
                            <span className='ellipsis'>{user.id}</span>
                        </td>
                        <td>{user.name}</td>
                        <td>{user.age}</td>
                        <td>{this.getFormattedDate(user.dob)}</td>
                        <td>
                            <i
                                data-toggle="tooltip"
                                title={"delete user " + user.name}
                                id={user.id}
                                className="fa fa-trash"
                                onClick={this.deleteUser}></i>
                        </td>
                    </tr>
                )
            })
        }
        return (
            <div className="data-table">
                <TableSimple
                    title="UserProfiles"
                    subTitle=""
                    data={users}
                    headers={headers}
                    columns="id.name.dob.age"
                    arrayOption={[
                    ["size", 'all', ', ']
                ]}/>
            </div>
        );
    }
}

// <table id='example' class='hidden table table-striped table-bordered'
// width='100%'> <thead>     <tr>         <th>Id</th>         <th>Name</th>
// <th>Dob</th>         <th>Age</th>         <th>Action</th>     </tr> </thead>
// <tbody>     {UserNode} </tbody> </table>