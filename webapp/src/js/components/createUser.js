import React from "react";
import DatePicker from 'react-bootstrap-date-picker';
export default class createUser extends React.Component {

    closeModal() {
        this
            .refs
            .closeButton
            .click();
    }

    render() {
        if (this.props.closeModal) {
            this.closeModal();
        }
        const {users} = this.props;
        return (
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Create User</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="email">Id</label>
                                <input
                                    className="form-control"
                                    type='text'
                                    name="id"
                                    onChange={this.props.setUser}
                                    placeholder='Enter Id'/>
                            </div>
                            <div class="form-group">
                                <label for="pwd">UserName</label>
                                <input
                                    className="form-control"
                                    type='text'
                                    name="name"
                                    onChange={this.props.setUser}
                                    placeholder='Enter Name'/>
                            </div>
                            <div class="form-group">
                                <label for="pwd">Email</label>
                                <input
                                    className="form-control"
                                    type='text'
                                    name="email"
                                    onChange={this.props.setUser}
                                    placeholder='Enter your email'/>
                            </div>
                            <div class="form-group">
                                <label for="pwd">Dob</label>
                                <DatePicker
                                    className='form-control'
                                    id='example-datepicker'
                                    value={this.props.user.dob}
                                    name="dob"
                                    onChange={this.props.setUser}
                                    placeholder='Enter Date of Birth'/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button className='btn btn-success' onClick={this.props.createUser}>
                                Add User</button>
                            <button
                                type="button"
                                ref='closeButton'
                                class="btn btn-danger fl-left"
                                data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}