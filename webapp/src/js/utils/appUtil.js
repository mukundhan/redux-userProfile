import Cookie from 'universal-cookie';
import {browserHistory} from 'react-router';
export function calculateYearsBetween(dateString) {
    var today = new Date();
    var givenDate = new Date(dateString);
    var yearsDifference = today.getFullYear() - givenDate.getFullYear();
    var month = today.getMonth() - givenDate.getMonth();
    if (month < 0 || (month === 0 && today.getDate() < givenDate.getDate())) {
        yearsDifference--;
    }
    return yearsDifference;
    path
};

export function setCookie(obj) {
    var cookie = new Cookie();
    var today = new Date();
    var cookieTimeOut = today;
    cookieTimeOut.setTime(today.getTime() + (obj.minutes * 60 * 1000));
    cookie.set(obj.name, obj.data, {
        path: '/',
        expires: cookieTimeOut
    });
    console.log("cookie", cookie.get(obj.name));
};

export function getCookie(name) {
    var cookie = new Cookie();
    return cookie.get(name);
};

export function removeCookie(name) {
    var cookie = new Cookie();
    return cookie.remove(name);
};

export function navigateTo(path) {
    console.log("Navigate To Path ", path);
    browserHistory.push(path);
}