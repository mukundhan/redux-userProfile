import DatePicker from 'react-bootstrap-date-picker';
import React from 'react';
import {Link} from 'react-router';
import {browserHistory} from 'react-router'
import {ToastContainer, toast} from 'react-toastify';
import * as userActions from '../actions/userActions';
import * as appUtil from '../utils/appUtil';
import LoginForm from '../containers/loginForm'
import UserProfile from '../containers/userProfile'
import Cookies from 'universal-cookie';
import {checkAuthorised} from '../services/authorisationService'

export default class Index extends React.Component {
  constructor(props) {
    super(props);
    console.log("checking for authorisation");
    checkAuthorised();
  }
  componentWillMount() {
    console.log('Will Mount called');
  }

  componentDidMount = () => {
    console.log('did Mount called');
  }

  componentWillUnmount() {
    console.log('Will Un-Mount called');
  }

  componentWillReceiveProps() {
    console.log("checking for authorisation");
    checkAuthorised();
  }

  render() {
    return (
      <div>
        <UserProfile/>
      </div>
    )
  }
}