import DatePicker from 'react-bootstrap-date-picker';
import React from 'react';
import constants from '../constants/constants';
import {Link} from 'react-router';
import {browserHistory} from 'react-router'
import {ToastContainer, toast} from 'react-toastify';
import * as userActions from '../actions/userActions';
import * as appUtil from '../utils/appUtil';
import LoginForm from '../containers/loginForm'
import Cookies from 'universal-cookie';
import {checkLogin, checkAuthorised} from '../services/authorisationService'

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        console.log("checking for User Logged In", checkLogin());
        if (checkLogin()) {
            appUtil.navigateTo(constants.HOME_PAGE);
        }
    }
    componentWillMount() {
        console.log('Will Mount called');
    }

    componentDidMount = () => {
        console.log('did Mount called');
    }

    componentWillReceiveProps() {
        console.log("checking for User Logged In");
        if (checkLogin()) {
            appUtil.navigateTo(constants.HOME_PAGE);
        }
    }

    componentWillUnmount() {
        console.log('Will Un-Mount called');
    }

    render() {
        return (
            <div>
                <LoginForm/>
            </div>
        )
    }
}