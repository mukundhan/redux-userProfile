import constants from '../constants/constants';
import * as API from '../API/applicationAPI';
import env from '../env/environmentalVariables';
import cookie from 'universal-cookie';
import * as appUtil from '../utils/appUtil';
// let config = {     headers: {         'Authorization': cookie.get()     } }

export function fetchUserProfiles() {
    return function (dispatch) {
        const tokenId = appUtil.getCookie("tokenId");
        dispatch({type: "FETCH_USERS"});
        API
            .get(env.url + "/userProfiles?access_token=" + tokenId)
            .then((response) => {
                dispatch({
                    type: "FETCH_USERS_FULFILLED",
                    payload: {
                        users: response.data
                    }
                })
            })
            .catch((err) => {
                dispatch({type: "FETCH_USERS_REJECTED", payload: err})
            })
    }
}

export function authenticateUser(user) {
    return function (dispatch) {
        API
            .get(env.url + "/Users/login", user)
            .then((response) => {
                dispatch({type: "FETCH_USERS_FULFILLED", payload: response.data})
            })
            .catch((err) => {
                dispatch({type: "FETCH_USERS_REJECTED", payload: err})
            })
    }
}

export function setUsers(users, dispatch) {
    dispatch({type: constants.SET_USERS, users: users});
}

export function createUser(user, callback) {
    return function (dispatch) {
        dispatch({type: "CREATE_USER"});
        const tokenId = appUtil.getCookie("tokenId");
        setTimeout(function () {
            API
                .post(env.url + "/userProfiles?access_token=" + tokenId, user)
                .then((response) => {
                    dispatch({
                        type: "CREATE_USER_FULFILLED",
                        payload: {
                            user: response.data
                        }
                    })
                    callback();
                })
                .catch((err) => {
                    console.log("error", err);
                    dispatch({type: "CREATE_USER_REJECTED", payload: err});
                    callback();
                })
        }, 2000)

    }
}

export function deleteUser(id, dispatch) {
    dispatch({type: constants.DELETE_USER, id: id});
}

export function updateUser(user, dispatch) {
    dispatch({type: constants.UPDATE_USER, user});
}