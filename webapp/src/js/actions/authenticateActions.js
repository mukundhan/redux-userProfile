import * as appUtil from '../utils/appUtil';
import constants from '../constants/constants';
import * as API from '../API/applicationAPI';
import env from '../env/environmentalVariables';
import cookie from 'universal-cookie';

export function login(user) {
    return function (dispatch) {
        dispatch({type: "LOGIN"});
        API
            .post(env.url + "/appusers/login", user)
            .then((response) => {
                if (response.status == 200) {
                    appUtil.setCookie({cookie, data: response.data.id, name: "tokenId", minitues: 60});
                    appUtil.navigateTo(constants.HOME_PAGE);
                    dispatch({
                        type: "LOGIN_FULFILLED",
                        payload: {
                            user: response.data,
                            authenticated: true,
                            role: "admin"
                        }
                    })
                } else {
                    dispatch({
                        type: "LOGIN_REJECTED",
                        payload: {
                            message: "login failed"
                        }
                    })
                }
            })
            .catch((err) => {
                dispatch({
                    type: "LOGIN_REJECTED",
                    payload: {
                        message: err.message
                    }
                })
            })
    }
}

export function logout(tokenId) {
    return function (dispatch) {
        dispatch({type: "LOGOUT"});
        API
            .post(env.url + "/appusers/logout?access_token=" + tokenId)
            .then((response) => {
                if (response.status == 204) {
                    dispatch({
                        type: "LOGOUT_FULFILLED",
                        payload: {
                            message: "success"
                        }
                    })
                } else {
                    dispatch({
                        type: "LOGOUT_REJECTED",
                        payload: {
                            message: "logout failed"
                        }
                    })
                }
                appUtil.removeCookie("tokenId");
                appUtil.navigateTo(constants.LOGIN_PAGE);
            })
            .catch((err) => {
                console.log("logout error", err)
                dispatch({
                    type: "LOGOUT_REJECTED",
                    payload: {
                        message: err.message
                    }
                })
                appUtil.removeCookie("tokenId");
            })
    }

}