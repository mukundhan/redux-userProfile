import axios from 'axios';
export function get(url) {
    return axios.get(url)
}
export function post(url, data) {
    console.log("user", data);
    return axios.post(url, data)
}
export function put(url, data) {
    return axios.put(url, data)
}
export function remove(url, id) {
    return axios.delete(url + "/" + id)
}
export function update(url, data) {
    return axios.patch(url + "/" + data.id, data)
}