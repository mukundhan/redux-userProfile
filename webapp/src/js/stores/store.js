import {applyMiddleware, createStore} from "redux"
import {createLogger} from "redux-logger"
import thunk from "redux-thunk"
import promise from "redux-promise-middleware"
import rootReducer from "./../reducers/rootReducer"
const middleware = applyMiddleware(thunk, createLogger(), promise());
export default createStore(rootReducer, middleware);

