import DatePicker from 'react-bootstrap-date-picker';
import {connect} from 'react-redux';
import React from 'react';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router';
import {ToastContainer, toast} from 'react-toastify';
import * as userActions from '../actions/userActions';
import * as authenticateActions from '../actions/authenticateActions'
import * as appUtil from '../utils/appUtil';
import CreateUser from '../components/createUser'
import ListUsers from '../components/users'
import {ClipLoader} from 'react-spinners';
class UserProfile extends React.Component {
    constructor(props) {
        super(props);
        console.log("props in userProfiles", this.props);
        this.state = {
            users: this.props.data.userProfile.users,
            closeModal: false,
            loading: this.props.data.userProfile.fetching
        }
        this.user = {};
    }

    createUser = () => {
        const {name, id, dob, email} = this.user;
        if (name && id && dob && email) {
            this
                .props
                .createUser(this.user, this.closeSpinner);
        } else {
            alert("Enter all the field to add User..");
        }
    }

    closeSpinner = () => {
        this.setState({closeModal: true, loading: false});
    }

    componentWillMount() {
        console.log('Will Mount called');
    }

    deleteUser = (event) => {
        userActions.deleteUser(event.target.id);
    }

    componentDidMount = () => {
        console.log('did Mount called');
        this
            .props
            .fetchUsers();
    }

    componentWillUpdate = (nextProps, nextState) => {}

    componentWillUnmount() {
        console.log('Will Un-Mount called');
    }

    componentWillReceiveProps(nextProps) {
        console.log("nextProps in userProfiles Page ", nextProps);
        this.setState({users: nextProps.data.userProfile.users, loading: nextProps.data.userProfile.fetching});
    }

    notify = (message, type) => {
        switch (type) {
            case 'success':
                {
                    toast.success(message);
                    break;
                }
            case 'error':
                {
                    toast.error(message);
                    break;
                }
            case 'warn':
                {
                    toast.warn(message);
                    break;
                }
            case 'info':
                {
                    toast.info(message);
                    break;
                }
        }
    }

    logout = () => {
        const tokenId = appUtil.getCookie("tokenId");
        console.log("tokenId in userProfiles", tokenId);
        this
            .props
            .logout(tokenId);
    }

    setUser = (event) => {
        if (event.target) {
            switch (event.target.name) {
                case 'id':
                    {
                        this.user.id = event.target.value;
                        break;
                    }
                case 'email':
                    {
                        this.user.email = event.target.value;
                        break;
                    }
                case 'name':
                    {
                        this.user.name = event.target.value;
                        break;
                    }
            }
        } else {
            this.user.dob = event;
            this.user.age = appUtil.calculateYearsBetween(event);
        }
        console.log("setUser", this.user);
    }

    render() {
        return (
            <div>
                <ClipLoader color={'#5bc0de'} size={40} loading={this.state.loading}/>
                <div className='data-table'>
                    <button className='btn btn-danger logout' onClick={this.logout}>
                        Logout</button>
                    <button
                        type="button"
                        class="btn btn-info add-user"
                        data-toggle="modal"
                        data-target="#myModal">Create new User</button>
                    <ToastContainer
                        position="top-right"
                        type="default"
                        autoClose={1000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        pauseOnHover/>
                    <CreateUser
                        setUser={this.setUser}
                        user={this.user}
                        createUser={this.createUser}
                        closeModal={this.state.closeModal}/>
                    <ListUsers users={this.state.users}/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({data: state})

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        createUser: userActions.createUser,
        logout: authenticateActions.logout,
        fetchUsers: userActions.fetchUserProfiles
    }, dispatch)
}

const userProfileContainer = connect(mapStateToProps, mapDispatchToProps,)(UserProfile)
export default userProfileContainer;