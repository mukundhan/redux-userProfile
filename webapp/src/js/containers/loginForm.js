import {connect} from 'react-redux';
import React from 'react';
import {bindActionCreators} from 'redux';
import DatePicker from 'react-bootstrap-date-picker';
import {Link} from 'react-router';
import {ToastContainer, toast} from 'react-toastify';
import * as authenticateActions from '../actions/authenticateActions';
import * as userActions from '../actions/userActions';
import * as appUtil from '../utils/appUtil';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        console.log("props in login form", this.props);
        this.user = {};
        this.username;
        this.password;
    }

    componentWillMount() {
        console.log('Will Mount called');
    }

    componentDidMount = () => {
        console.log('did Mount called');
    }

    componentWillUnmount() {
        console.log('Will Un-Mount called');
    }

    login(username, password) {
        if (username && password) {
            this.user.username = username;
            this.user.password = password;
            this
                .props
                .userLogin(this.user);
            console.log("user", this.user);
        } else {
            alert("Enter userName and Password to login..");
        }
    }

    setUser = (event) => {
        if (event.target) {
            switch (event.target.name) {
                case 'name':
                    {
                        this.user.username = event.target.value;
                        break;
                    }
                case 'password':
                    {
                        this.user.password = event.target.value;
                        break;
                    }
            }
            console.log("user", this.user);
        }
    }

    render() {
        let username,
            password;
        return (
            <div>
                <div class="data-table login-fm">
                    <h2>Login</h2>

                    <div class="form-group">
                        <label for="email">UserName</label>
                        <input
                            type="text"
                            class="form-control"
                            ref={node => {
                            username = node
                        }}
                            id="email"
                            name="name"
                            placeholder="Enter email"
                            onChange={this.setUser}/>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password</label>
                        <input
                            type="password"
                            class="form-control"
                            id="pwd"
                            name="password"
                            ref={node => {
                            password = node
                        }}
                            placeholder="Enter password"
                            onChange={this.setUser}/>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"/>
                            Remember me</label>
                    </div>
                    <button
                        class="btn btn-success"
                        onClick={() => this.login(username.value, password.value)}>Submit</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({data: state})

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        userLogin: authenticateActions.login
    }, dispatch)
}

const LoginFormContainer = connect(mapStateToProps, mapDispatchToProps,)(LoginForm)
export default LoginFormContainer;