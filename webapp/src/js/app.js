import {createLogger} from 'redux-logger'
import thunk from 'redux-thunk'
import {createStore} from 'redux'
import React from "react"
import ReactDOM from "react-dom"
import {Router, Route, IndexRoute, browserHistory} from "react-router"
import {Provider} from 'react-redux'
import Index from "./pages/Index"
import Login from "./pages/Login"
import store from './stores/store'
import createHashHistory from 'history/lib/createHashHistory';
const history = createHashHistory({queryKey: false})
const app = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
  <Router history={browserHistory}>
    <Route path="/" component={Login}></Route>
    <Route path="/login" component={Login}></Route>
    <Route path="/userProfiles" component={Index}></Route>
  </Router>
</Provider>, app);